from dashboard import models as dashboard_models
BOT_NAME = "LibraryBot"

BOT_TOKEN = '814555937:AAEoEU2aS3Z4FPJokuYeqzBbY2uEThADK1w'

BOT_URL = 'https://api.telegram.org/bot{token}/'.format(token=BOT_TOKEN)

SEND_MESSAGE_URL = "{bot_url}sendMessage".format(bot_url=BOT_URL)

ALLOWED_CHATS = [-1001301583551]

# Split using everything that isn't a letter, number, single quotes
# Double quotes, underscore or hyphen
REGEX_PATTERN_TO_SPLIT_MESSAGE_TEXT_INTO_WORDS = r"[^a-zA-Z0-9'\"_-]"


ACCEPT_MULTI_WORD_FIELDS_AS_QUOTATIONS = True

REGEX_PATTERN_TO_IDENTIFY_WORDS_IN_SINGLE_QOUTES = "'[^']+'"
REGEX_PATTERN_TO_IDENTIFY_WORDS_IN_DOUBLE_QOUTES = '"[^"]+"'

REGEX_PATTERN_TO_IDENTIFY_WORDS_IN_QOUTES = "{single}|{double}".format(
                                                                        single=REGEX_PATTERN_TO_IDENTIFY_WORDS_IN_SINGLE_QOUTES,
                                                                        double=REGEX_PATTERN_TO_IDENTIFY_WORDS_IN_DOUBLE_QOUTES,
                                                                       )

BOOK_MODEL = dashboard_models.Book
NAME_OF_BOOK_TABLE_IN_DATABASE = "dashboard_book"
