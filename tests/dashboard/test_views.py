from django.test import TestCase, Client
from django.urls import reverse

from dashboard.models import Book


from tests.dashboard.helpers import DEMO_SLUG
from tests.dashboard.helpers import ABSOLUTE_FILE_PATH, DEMO_TITLE, DEMO_AUTHOR
from tests.dashboard.helpers import ANOTHER_DEMO_TITLE, ANOTHER_DEMO_AUTHOR


class TestDashboardViews(TestCase):
    def setUp(self):
        self.client = Client()

        self.book_list_url = reverse("dashboard-home")
        self.book_create_url = reverse("dashboard-upload-book")
        self.book_update_url = reverse("dashboard-edit-book", kwargs={"slug": "default-by-default"})
        self.book_delete_url = reverse("dashboard-delete-book", kwargs={"slug": "default-by-default"})

        with open(ABSOLUTE_FILE_PATH) as file_object:
            data = {
                "title": "default",
                "author": "default",
                "subject": "Demo Subject",
                "details": "Demo Details",
                "file": file_object
            }
            response = self.client.post(self.book_create_url, data)

    def test_book_list_view_using_get_method(self):
        response = self.client.get(self.book_list_url)

        self.assertTemplateUsed(response, "dashboard/book_list.html")
        self.assertEquals(response.status_code, 200)

    def test_book_create_view_using_post_method(self):
        with open(ABSOLUTE_FILE_PATH) as file_object:
            data = {
                "title": DEMO_TITLE,
                "author": DEMO_AUTHOR,
                "subject": "Demo Subject",
                "details": "Demo Details",
                "file": file_object
            }
            response = self.client.post(self.book_create_url, data)

            self.assertRedirects(response, self.book_list_url)

    def test_book_update_view_using_put_method(self):
        with open(ABSOLUTE_FILE_PATH) as file_object:
            data = {
                "title": "default",
                "author": "default",
                "subject": "Default Subject",
                "details": "Default Details",
                "file": file_object
            }
            response = self.client.put(self.book_update_url, data)

            self.assertTemplateUsed(response, "dashboard/book_form.html")
            self.assertEquals(response.status_code, 200)

    def test_book_delete_view_using_get_method(self):
        response = self.client.get(self.book_delete_url)

        self.assertTemplateUsed(response, "dashboard/book_confirm_delete.html")
        self.assertEquals(response.status_code, 200)

    def test_book_delete_view_using_delete_method(self):
        response = self.client.delete(self.book_delete_url)

        self.assertRedirects(response, self.book_list_url)
