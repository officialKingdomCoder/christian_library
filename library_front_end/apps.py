from django.apps import AppConfig


class LibraryFrontEndConfig(AppConfig):
    name = 'library_front_end'
