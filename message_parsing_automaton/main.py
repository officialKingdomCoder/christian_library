import re as regex

from message_parsing_automaton.entities import State, FieldState, Sequence, Switch

from message_parsing_automaton.setup import TEMPLATES_OF_ACCEPTABLE_MESSAGES
from message_parsing_automaton.setup import REGEX_PATTERN_TO_SPLIT_TEMPLATES_OF_ACCEPTABLE_MESSAGES_INTO_TOKENS
from message_parsing_automaton.setup import PATTERN_FOR_FIELDS_IN_TEMPLATES

from message_parsing_automaton.exceptions import AllTokensHaveBeenProcessed
from message_parsing_automaton.exceptions import TokenDoesNotMatchState


def extract_fields_using_automaton(text_tokens):
    automaton_to_extract_fields_with_from_text = generate_automaton(TEMPLATES_OF_ACCEPTABLE_MESSAGES)
    fields = find_fields_present_in_text_tokens(text_tokens, automaton_to_extract_fields_with_from_text)
    return fields


def generate_automaton(message_patterns):
    collection_of_sequence_objects_generated_from_each_pattern = create_sequences(message_patterns)
    automaton = merge_sequences(collection_of_sequence_objects_generated_from_each_pattern)
    return automaton


def create_sequences(message_patterns):
    sequences_of_consecutive_automaton_states = []

    for message_pattern in message_patterns:
        sequences_of_consecutive_automaton_states.append(create_sequence_for_individual_message_pattern(message_pattern))

    return sequences_of_consecutive_automaton_states


def create_sequence_for_individual_message_pattern(message_pattern):
    sequence_object = Sequence()

    tokens = split_message_pattern_into_tokens(message_pattern)

    for token in tokens:
        state_object = convert_token_to_state_or_field_state_object(token)
        if state_object:
            sequence_object.append(state_object)

    return sequence_object


def split_message_pattern_into_tokens(message_pattern):
    message_pattern = message_pattern.lower()
    tokens = regex.split(REGEX_PATTERN_TO_SPLIT_TEMPLATES_OF_ACCEPTABLE_MESSAGES_INTO_TOKENS, message_pattern)
    return tokens


def convert_token_to_state_or_field_state_object(token):
    if token is not "":
        regex_match_object_for_result = regex.match(PATTERN_FOR_FIELDS_IN_TEMPLATES, token)

        if (regex_match_object_for_result):
            field_name = get_field_name_from_regex_match_object(regex_match_object_for_result)
            return FieldState(pattern=PATTERN_FOR_FIELDS_IN_TEMPLATES, field_name=field_name)
        else:
            pattern = generate_pattern_that_matches_exactly_the_token_text(token)
            return State(pattern=pattern)


def get_field_name_from_regex_match_object(regex_match_object):
    match_text = regex_match_object.group()
    field_name = strip_curly_braces(match_text)
    return field_name


def strip_curly_braces(text):
    text = text.strip("{")
    text = text.strip("}")
    return text


def generate_pattern_that_matches_exactly_the_token_text(token_text):
    return r"^{}$".format(token_text)


def merge_sequences(two_dimensional_list_with_rows_as_sequences):
    length_of_longest_sequence = get_max_sequence_length(two_dimensional_list_with_rows_as_sequences)
    column_indexes = range(length_of_longest_sequence)

    merged_sequence = create_merged_sequence(two_dimensional_list_with_rows_as_sequences, column_indexes)

    return merged_sequence


def get_max_sequence_length(two_dimensional_list_with_rows_as_sequences):
    return max([len(sequence) for sequence in two_dimensional_list_with_rows_as_sequences])


def create_merged_sequence(two_dimensional_list_with_rows_as_sequences, column_indexes):
    merged_sequence = Sequence()

    for current_column in column_indexes:
        states_at_current_column_index = get_states_at_current_column_index(two_dimensional_list_with_rows_as_sequences, current_column)

        if states_at_current_column_index:
            next_object = get_next_object_to_be_added_to_the_sequence(two_dimensional_list_with_rows_as_sequences, current_column, states_at_current_column_index)

            merged_sequence.append(next_object)

            if isinstance(next_object, Switch):
                break

    return merged_sequence


def get_states_at_current_column_index(two_dimensional_list, current_column):
    return [row[current_column] for row in two_dimensional_list if current_column < len(row)]


def get_next_object_to_be_added_to_the_sequence(two_dimensional_list_with_rows_as_sequences, current_column, states_at_current_column_index):
    states_are_equal = check_if_states_are_equal(states_at_current_column_index)

    if (states_are_equal):
        state_at_index = return_single_instance_of_state(states_at_current_column_index)
        return state_at_index
    else:
        unchecked_subset_of_two_dimensional_list = extract_the_unchecked_columns(two_dimensional_list_with_rows_as_sequences, current_column)

        groups = collect_subset_of_sequence_list_into_groups_based_on_equal_states(unchecked_subset_of_two_dimensional_list)

        decision_point = create_switch_object_decision_point_from_groups(groups)

        return decision_point


def check_if_states_are_equal(states):
    if len(states) < 2:
        return True

    else:
        indexes_till_penultimate_state = range(len(states) - 1)

        for index in indexes_till_penultimate_state:
            states_to_compare = (states[index], states[index + 1])

            equality = compare_equality_of_types_of_two_states(states_to_compare)
            if equality is False:
                return equality

            # child class FieldState checked before parent class State
            if isinstance(states[index], FieldState):
                equality = compare_equality_of_two_field_states(states_to_compare)
                if equality is False:
                    return equality

            elif isinstance(states[index], State):
                equality = compare_equality_of_two_states(states_to_compare)
                if equality is False:
                    return equality
            else:
                return False
    return True


def compare_equality_of_types_of_two_states(states):
    if type(states[0]) is not type(states[1]):
        return False
    return True


def compare_equality_of_two_field_states(states):
    if states[0].pattern != states[1].pattern:
        return False
    if states[0].field_name != states[1].field_name:
        return False
    return True


def compare_equality_of_two_states(states):
    if states[0].pattern != states[1].pattern:
        return False
    return True


def return_single_instance_of_state(states):
    return states[0]


def extract_the_unchecked_columns(two_dimensional_list, current_column):
    return [row[current_column:] for row in two_dimensional_list]


def collect_subset_of_sequence_list_into_groups_based_on_equal_states(two_dimensional_subset_of_sequence_list):
    groups = {}
    if two_dimensional_subset_of_sequence_list:
        decision_column = extract_first_column(two_dimensional_subset_of_sequence_list)

        distinct_states_in_decision_column = generate_list_of_distinct_states(decision_column)

        groups = generate_empty_groups_from_distinct_states(distinct_states_in_decision_column)

        groups = add_each_sequence_to_its_group(two_dimensional_subset_of_sequence_list, groups)

    return groups


def extract_first_column(two_dimensional_subset_of_sequence_list):
    return [sequence[0] for sequence in two_dimensional_subset_of_sequence_list if sequence]


def generate_list_of_distinct_states(states):
    distinct_states = []
    for state in states:
        new_state = True
        for distinct_state in distinct_states:
            states_are_equal = check_if_states_are_equal([state, distinct_state])
            if states_are_equal:
                new_state = False
                break
        if new_state:
            distinct_states.append(state)
    return distinct_states


def generate_empty_groups_from_distinct_states(distinct_states):
    groups_with_empty_lists = {}

    for distinct_state in distinct_states:
        groups_with_empty_lists[distinct_state] = []

    return groups_with_empty_lists


def add_each_sequence_to_its_group(two_dimensional_subset_of_sequence_list, groups):
    for sequence in two_dimensional_subset_of_sequence_list:
        if sequence:
            for group in groups:
                if check_if_states_are_equal([sequence[0], group]):
                    groups[group].append(sequence)
                    break
    return groups


def create_switch_object_decision_point_from_groups(groups):
    decision_point = Switch()

    for group in groups:
        result = merge_sequences(groups[group])
        decision_point[group] = result

    return decision_point


def find_fields_present_in_text_tokens(text_tokens, automaton):
    try:
        fields_present_in_the_text = {}
        for index_of_current_state in range(len(automaton)):
            handle_all_tokens_have_been_processed_but_end_of_automaton_has_not_been_reached(text_tokens, index_of_current_state)

            current_state_is_a_decision_point = isinstance(automaton[index_of_current_state], Switch)
            if current_state_is_a_decision_point:
                fields_in_selected_branch = extract_fields_in_branch_selected(automaton, text_tokens, index_of_current_state)

                fields_present_in_the_text.update(fields_in_selected_branch)

            else:
                field_in_state = extract_field_in_state(automaton, text_tokens, index_of_current_state)
                fields_present_in_the_text.update(field_in_state)

        return fields_present_in_the_text

    except AllTokensHaveBeenProcessed:
        return fields_present_in_the_text

    except TokenDoesNotMatchState:
        return fields_present_in_the_text


def handle_all_tokens_have_been_processed_but_end_of_automaton_has_not_been_reached(text_tokens, index_of_current_state):
    all_tokens_have_been_processed = (index_of_current_state >= len(text_tokens))
    if all_tokens_have_been_processed:
        raise AllTokensHaveBeenProcessed



def extract_fields_in_branch_selected(automaton, text_tokens, index_of_current_state):
    decision_point = automaton[index_of_current_state]
    current_token = text_tokens[index_of_current_state]
    tokens_left_to_be_matched = text_tokens[index_of_current_state:]

    branch_to_follow = choose_branch_to_follow(decision_point, current_token)

    if branch_to_follow:

        fields_in_branch = follow_branch(branch_to_follow, tokens_left_to_be_matched)

        if fields_in_branch:
            return fields_in_branch
        else:
            return {}


def choose_branch_to_follow(decision_point, current_token):
    for possible_branch in decision_point:
        pattern_of_current_branch_being_checked = get_pattern_of_current_branch(possible_branch)

        regex_match_object = regex.match(pattern_of_current_branch_being_checked, current_token)

        if (regex_match_object):
            branch_to_follow = decision_point[possible_branch]
            return branch_to_follow


def get_pattern_of_current_branch(possible_branch):
    pattern = possible_branch.pattern
    pattern = strip_curly_braces(pattern)
    return pattern


def follow_branch(branch_to_follow, tokens_left_to_be_matched):
    fields_present_in_the_rest_of_the_tokens = find_fields_present_in_text_tokens(tokens_left_to_be_matched, branch_to_follow)

    return fields_present_in_the_rest_of_the_tokens


def extract_field_in_state(automaton, text_tokens, index_of_current_state):
    pattern_of_current_state = get_pattern_of_current_state(automaton, index_of_current_state)
    current_token = text_tokens[index_of_current_state]

    regex_match_object = regex.match(pattern_of_current_state, current_token)

    if (regex_match_object):
        current_state = automaton[index_of_current_state]
        current_state_is_a_field_state = isinstance(current_state, FieldState)

        if current_state_is_a_field_state:
            field = return_field(current_state, regex_match_object)

            return field
        else:
            return {}
    else:
        raise TokenDoesNotMatchState


def return_field(current_state, regex_match_object):
    field_name = current_state.field_name
    field_value = regex_match_object.group()

    field = {field_name: field_value}
    return field


def get_pattern_of_current_state(automaton, index_of_current_state):
    pattern_of_current_state = automaton[index_of_current_state].pattern
    pattern_of_current_state = strip_curly_braces(pattern_of_current_state)

    return pattern_of_current_state
