from django.urls import path
from telegram_bot.views import BotView


urlpatterns = [
    path("", BotView.as_view(), name="bot-endpoint"),
]
