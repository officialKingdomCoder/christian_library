"""
from telegram_bot.helpers import ChatIdNotAllowed, MessageDoesNotHaveText
from telegram_bot.helpers import BotIsNotBeingAddressed, MatchingBooksNotFound
from telegram_bot.exceptions import MessagesFromBotForbidden
"""


class MessageIsMissing(Exception):
    def __init__(self):
        super(MessageIsMissing, self).__init__(self, "Chat id field was missing in request received")


class InvalidRequest(Exception):
    def __init__(self):
        Exception.__init__(self, "Request is malformed. Key field is missing")


class ChatIdNotAllowed(Exception):
    def __init__(self, chat_id):
        Exception.__init__(self, "Conversations are not allowed with the bot from chat ID {chat_id}".format(chat_id=chat_id))
        self.chat_id = chat_id


class MessagesFromBotForbidden(Exception):
    def __init__(self):
        Exception.__init__(self, "Messages from other bots are forbidden")


class MessageDoesNotHaveText(Exception):
    def __init__(self):
        Exception.__init__(self, "The message received does not have a text field")


class BotIsNotBeingAddressed(Exception):
    def __init__(self, bot_name):
        message = "{bot_name} is not being addressed. Send message using appropriate format".format(bot_name=bot_name)
        Exception.__init__(self, message)


class NoBookParameterSpecified(Exception):
    def __init__(self):
        Exception.__init__(self, "The message received does not have any book parameter specified")


class MatchingBooksNotFound(Exception):
    def __init__(self, chat_id, book_parameters):
        Exception.__init__(self, "There is currently no book matching the following parameters\n{book_parameters}".format(book_parameters=book_parameters))
        self.chat_id = chat_id
        self.book_parameters = book_parameters
