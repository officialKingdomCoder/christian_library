from django.views.generic import ListView, DetailView
from django.utils.decorators import method_decorator
from django.views.decorators.csrf import csrf_exempt

from dashboard.models import Book


# remove this
@method_decorator(csrf_exempt, name='dispatch')
class FrontEndSearchResultsView(ListView):
    model = Book
    template_name = "library_front_end/search_results.html"

    def post(self, request, *args, **kwargs):
        self.queryset = request.POST["queryset"]

    def get_queryset(self):
        parameters = {}

        for field in self.request.GET:
            key = "{field}__icontains".format(field=field)
            value = self.request.GET[field]

            parameters.update({key: value})

        object_list = self.model.objects.filter(**parameters)

        print("# DEBUG: self.request.GET: {}".format(self.request.GET))
        print("# DEBUG: Parameters: {}".format(parameters))
        print("# DEBUG: Object List: {}".format(object_list))

        return object_list


# remove this
@method_decorator(csrf_exempt, name='dispatch')
class FrontEndBookDetails(DetailView):
    model = Book
    template_name = "library_front_end/book_details.html"
