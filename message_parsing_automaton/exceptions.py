class AllTokensHaveBeenProcessed(Exception):

    def __init__(self):
        message = "All tokens have been processed"
        super(AllTokensHaveBeenProcessed, self).__init__(message)


class TokenDoesNotMatchState(Exception):

    def __init__(self):
        message = "Token does not match state"
        super(TokenDoesNotMatchState, self).__init__(message)
