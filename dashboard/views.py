from django.urls import reverse_lazy

from django.views.generic import ListView, CreateView, UpdateView, DeleteView

from dashboard.models import Book


# Create your views here.
class BookListView(ListView):
    model = Book
    template_name = 'dashboard/templates/book_list.html'
    paginate_by = 20
    ordering = ['title']


class BookCreateView(CreateView):
    model = Book
    template_name = 'dashboard/templates/book_form.html'
    fields = ['title', 'author', 'subject', 'details', 'file']
    success_url = reverse_lazy("dashboard-list-books")


class BookUpdateView(UpdateView):
    model = Book
    fields = ['title', 'author', 'subject', 'details', 'file']
    success_url = reverse_lazy("dashboard-list-books")


class BookDeleteView(DeleteView):
    model = Book
    success_url = reverse_lazy("dashboard-list-books")
