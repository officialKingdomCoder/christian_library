# Split using everything that is not a letter, number, open and close
# curly braces, underscore or hyphen
REGEX_PATTERN_TO_SPLIT_TEMPLATES_OF_ACCEPTABLE_MESSAGES_INTO_TOKENS = r"[^a-zA-Z0-9{}_-]"


TEMPLATES_OF_ACCEPTABLE_MESSAGES = [
                                  "{bot_name}, please, I need {title}",
                                  "{bot_name}, please, I need {title} by {author}",
                                  "{bot_name}, please, I need books by {author}",
                                  "{bot_name}, please, I need books by {author} on {subject}",
                                  "{bot_name}, please, I need books on {subject}"
                                 ]

PATTERN_FOR_FIELDS_IN_TEXT = r"[a-zA-Z0-9_\- ]+"

PATTERN_FOR_FIELDS_IN_TEMPLATES = "{" + PATTERN_FOR_FIELDS_IN_TEXT + "}"
