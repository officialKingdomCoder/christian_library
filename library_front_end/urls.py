from django.urls import path
from library_front_end.views import FrontEndSearchResultsView, FrontEndBookDetails


urlpatterns = [
    path("search_results/", FrontEndSearchResultsView.as_view(), name="search-results"),
    path("<slug>/", FrontEndBookDetails.as_view(), name="book-details"),
]
