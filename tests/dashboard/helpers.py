import os
from dashboard.models import Book
from django.utils.text import slugify
from django.core.files.uploadedfile import SimpleUploadedFile

DEMO_TITLE = "Demo Title"
DEMO_AUTHOR = "Demo Author"
DEMO_SLUG = slugify("{title} by {author}".format(title=DEMO_TITLE, author=DEMO_AUTHOR))


"""def create_demo_book():
    relative_file_path = './tests/dashboard/book.pdf'
    full_file_path = os.path.abspath(relative_file_path)
    file_name = os.path.basename(full_file_path)

    file_object = open(full_file_path, encoding='base64').read()
    file_object_for_db = SimpleUploadedFile(file_name, file_object)

    return Book.objects.create(
        title=DEMO_TITLE,
        author=DEMO_AUTHOR,
        subject="Demo Subject",
        details="Demo Details",
        file=file_object_for_db
    )
"""


"""def create_demo_book():
    return Book.objects.create(
        title=DEMO_TITLE,
        author=DEMO_AUTHOR,
        subject="Demo Subject",
        details="Demo Details",
        file="path/to/book"
    )"""


relative_file_path = './tests/dashboard/book.txt'
ABSOLUTE_FILE_PATH = os.path.abspath(relative_file_path)


def create_demo_book(file_object):
    return Book.objects.create(
        title=DEMO_TITLE,
        author=DEMO_AUTHOR,
        subject="Demo Subject",
        details="Demo Details",
        file=file_object
    )


ANOTHER_DEMO_TITLE = "Demo Title"
ANOTHER_DEMO_AUTHOR = "Demo Author"
ANOTHER_DEMO_SLUG = slugify("{title} by {author}".format(title=ANOTHER_DEMO_TITLE, author=ANOTHER_DEMO_AUTHOR))


"""with open(ABSOLUTE_FILE_PATH) as file_object:
    ANOTHER_DEMO_BOOK_DETAILS = {
        "title": ANOTHER_DEMO_TITLE,
        "author": ANOTHER_DEMO_AUTHOR,
        "subject": "Another Demo Subject",
        "details": "Another Demo Details",
        "file": file_object
    }

    UPDATE_ANOTHER_DEMO_BOOK_DETAILS = {
        "title": ANOTHER_DEMO_TITLE,
        "author": ANOTHER_DEMO_AUTHOR,
        "subject": "Another Demo Subject Updated",
        "details": "Another Demo Details Updated",
        "file": file_object
    }"""


"""
ANOTHER_DEMO_BOOK_DETAILS = {
    "title": ANOTHER_DEMO_TITLE,
    "author": ANOTHER_DEMO_AUTHOR,
    "subject": "Another Demo Subject",
    "details": "Another Demo Details",
    "file": file_object
}

UPDATE_ANOTHER_DEMO_BOOK_DETAILS = {
    "title": ANOTHER_DEMO_TITLE,
    "author": ANOTHER_DEMO_AUTHOR,
    "subject": "Another Demo Subject Updated",
    "details": "Another Demo Details Updated",
    "file": file_object
}
"""
