import os

DEMO_ACCEPTED_CHAT_ID = -1001301583551
DEMO_NOT_ACCEPTED_CHAT_ID = 100000009

MESSAGE_NOT_ADDRESSED_TO_BOT = "Please, I need Mastery by Robert Greene"

VALID_BOOK_REQUEST = "LibraryBot, please, I need Demo by Demo"
REQUEST_WITH_NO_BOOK_PARAMETER = "LibraryBot, hey"
REQUEST_FOR_BOOK_THAT_DOES_NOT_EXIST = "LibraryBot, please, I need fake_book by fake_author"

relative_file_path = './tests/telegram_bot/book.txt'
ABSOLUTE_FILE_PATH = os.path.abspath(relative_file_path)

MINIMALIZED_ACCEPTABLE_TELEGRAM_REQUEST = {
    "update_id": 646911460,
    "message": {
        "message_id": 93,
        "from": {
            "id": 999999999,
            "is_bot": False,
        },
        "chat": {
            "id": DEMO_ACCEPTED_CHAT_ID,
            "type": "supergroup"
        },
        "date": 1509641174,
        "text": VALID_BOOK_REQUEST
    }
}

MINIMALIZED_REQUEST_WITHOUT_MESSAGE = {
    "update_id": 646911460,
}

MINIMALIZED_REQUEST_WITHOUT_CHAT_ID = {
    "update_id": 646911460,
    "message": {
        "message_id": 93,
        "from": {
            "id": 999999999,
            "is_bot": False,
        },
        "chat": {
            "type": "supergroup"
        },
        "date": 1509641174,
        "text": VALID_BOOK_REQUEST
    }
}

MINIMALIZED_REQUEST_WITH_CHAT_ID_NOT_ALLOWED = {
    "update_id": 646911460,
    "message": {
        "message_id": 93,
        "from": {
            "id": 999999999,
            "is_bot": False,
        },
        "chat": {
            "id": DEMO_NOT_ACCEPTED_CHAT_ID,
            "type": "supergroup"
        },
        "date": 1509641174,
        "text": VALID_BOOK_REQUEST
    }
}

MINIMALIZED_REQUEST_FROM_A_BOT = {
    "update_id": 646911460,
    "message": {
        "message_id": 93,
        "from": {
            "id": 999999999,
            "is_bot": True,
        },
        "chat": {
            "id": DEMO_ACCEPTED_CHAT_ID,
            "type": "supergroup"
        },
        "date": 1509641174,
        "text": VALID_BOOK_REQUEST
    }
}

MINIMALIZED_REQUEST_WITHOUT_TEXT = {
    "update_id": 646911460,
    "message": {
        "message_id": 93,
        "from": {
            "id": 999999999,
            "is_bot": False,
        },
        "chat": {
            "id": DEMO_ACCEPTED_CHAT_ID,
            "type": "supergroup"
        },
        "date": 1509641174,
    }
}

MINIMALIZED_REQUEST_NOT_ADDRESSED_TO_BOT = {
    "update_id": 646911460,
    "message": {
        "message_id": 93,
        "from": {
            "id": 999999999,
            "is_bot": False,
        },
        "chat": {
            "id": DEMO_ACCEPTED_CHAT_ID,
            "type": "supergroup"
        },
        "date": 1509641174,
        "text": MESSAGE_NOT_ADDRESSED_TO_BOT
    }
}

MINIMALIZED_REQUEST_WITH_NO_BOOK_PARAMETERS = {
    "update_id": 646911460,
    "message": {
        "message_id": 93,
        "from": {
            "id": 999999999,
            "is_bot": False,
        },
        "chat": {
            "id": DEMO_ACCEPTED_CHAT_ID,
            "type": "supergroup"
        },
        "date": 1509641174,
        "text": REQUEST_WITH_NO_BOOK_PARAMETER
    }
}

MINIMALIZED_REQUEST_FOR_BOOK_THAT_DOES_NOT_EXIST = {
    "update_id": 646911460,
    "message": {
        "message_id": 93,
        "from": {
            "id": 999999999,
            "is_bot": False,
        },
        "chat": {
            "id": DEMO_ACCEPTED_CHAT_ID,
            "type": "supergroup"
        },
        "date": 1509641174,
        "text": REQUEST_FOR_BOOK_THAT_DOES_NOT_EXIST
    }
}
