from django.test import SimpleTestCase
from django.urls import reverse, resolve

from tests.dashboard.helpers import DEMO_SLUG


class TestDashboardURLs(SimpleTestCase):
    # TODO: I need a way to test that the admin site resolves

    def test_dashboard_home_url_resolves(self):
        home_view_name = "dashboard-home"
        home_url = reverse(home_view_name)

        self.assertURLEqual(resolve(home_url).view_name, home_view_name)

    def test_dashboard_upload_book_url_resolves(self):
        upload_book_view_name = "dashboard-upload-book"
        upload_book_url = reverse(upload_book_view_name)

        self.assertURLEqual(resolve(upload_book_url).view_name, upload_book_view_name)

    def test_dashboard_edit_book_url_resolves(self):
        edit_book_view_name = "dashboard-edit-book"
        edit_book_url = reverse(edit_book_view_name, kwargs={"slug": DEMO_SLUG})

        self.assertURLEqual(resolve(edit_book_url).view_name, edit_book_view_name)

    def test_dashboard_delete_book_url_resolves(self):
        delete_book_view_name = "dashboard-delete-book"
        delete_book_url = reverse(delete_book_view_name, kwargs={"slug": DEMO_SLUG})

        self.assertURLEqual(resolve(delete_book_url).view_name, delete_book_view_name)
