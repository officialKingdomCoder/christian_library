from django.views import View
from django.http import JsonResponse
from django.utils.decorators import method_decorator
from django.views.decorators.csrf import csrf_exempt

from telegram_bot.helpers import validate_message_is_present
from telegram_bot.helpers import get_relevant_parameters_from_telegram_message

from telegram_bot.helpers import validate_chat_id
from telegram_bot.helpers import send_chat_id_not_allowed_error_message_to_telegram

from telegram_bot.helpers import validate_message_is_not_from_another_bot

from telegram_bot.helpers import validate_message_has_text

from telegram_bot.helpers import extract_fields_from_message_text

from telegram_bot.helpers import validate_bot_is_being_addressed
from telegram_bot.helpers import remove_bot_name_from_fields

from telegram_bot.helpers import validate_book_parameters_were_specified

from telegram_bot.helpers import convert_queryset_to_list_of_dictionaries
from telegram_bot.helpers import find_books_in_database

from telegram_bot.helpers import validate_books_that_matched_query_were_found
from telegram_bot.helpers import send_no_matching_books_found_error_message_to_telegram

from telegram_bot.helpers import send_link_to_url_of_matching_books_to_telegram


from telegram_bot.exceptions import MessageIsMissing
from telegram_bot.exceptions import ChatIdNotAllowed
from telegram_bot.exceptions import MessagesFromBotForbidden
from telegram_bot.exceptions import InvalidRequest
from telegram_bot.exceptions import MessageDoesNotHaveText
from telegram_bot.exceptions import BotIsNotBeingAddressed
from telegram_bot.exceptions import NoBookParameterSpecified
from telegram_bot.exceptions import MatchingBooksNotFound


# remove this
@method_decorator(csrf_exempt, name='dispatch')
class BotView(View):
    def post(self, request, *args, **kwargs):
        try:
            validate_message_is_present(request)

            telegram_parameters = get_relevant_parameters_from_telegram_message(request)
            validate_chat_id(telegram_parameters)
            validate_message_is_not_from_another_bot(telegram_parameters)

            validate_message_has_text(telegram_parameters)

            fields_in_message_text = extract_fields_from_message_text(telegram_parameters)

            validate_bot_is_being_addressed(fields_in_message_text)
            fields_in_message_text = remove_bot_name_from_fields(fields_in_message_text)

            validate_book_parameters_were_specified(fields_in_message_text)

            database_queryset = find_books_in_database(fields_in_message_text)
            matching_books = convert_queryset_to_list_of_dictionaries(database_queryset)

            validate_books_that_matched_query_were_found(matching_books, telegram_parameters, fields_in_message_text)

            send_link_to_url_of_matching_books_to_telegram(request, matching_books, telegram_parameters, fields_in_message_text)
            return JsonResponse({"ok": "Books sent"})

        except MessageIsMissing as e:
            return JsonResponse({"error": "Message missing"})

        except ChatIdNotAllowed as e:
            send_chat_id_not_allowed_error_message_to_telegram(e.chat_id)
            return JsonResponse({"error": "Message not allowed from chat id to bot"})

        except MessagesFromBotForbidden as e:
            return JsonResponse({"error": "Messages from other bots are forbidden"})

        except InvalidRequest as e:
            return JsonResponse({"error": "Request is malformed. Key field is missing"})

        except MessageDoesNotHaveText as e:
            return JsonResponse({"error": "Message does not have text"})

        except BotIsNotBeingAddressed as e:
            return JsonResponse({"error": "Message is not addressed to bot"})

        except NoBookParameterSpecified as e:
            return JsonResponse({"error": "Message contains no book parameter"})

        except MatchingBooksNotFound as e:
            send_no_matching_books_found_error_message_to_telegram(e.chat_id, e.book_parameters)
            return JsonResponse({"error": "No book matched your query"})
"""

        """
