import json
from django.test import TestCase, Client
from django.urls import reverse

from tests.telegram_bot.helpers import ABSOLUTE_FILE_PATH

from tests.telegram_bot.helpers import MINIMALIZED_ACCEPTABLE_TELEGRAM_REQUEST
from tests.telegram_bot.helpers import MINIMALIZED_REQUEST_WITHOUT_MESSAGE
from tests.telegram_bot.helpers import MINIMALIZED_REQUEST_WITHOUT_CHAT_ID
from tests.telegram_bot.helpers import MINIMALIZED_REQUEST_WITH_CHAT_ID_NOT_ALLOWED
from tests.telegram_bot.helpers import MINIMALIZED_REQUEST_FROM_A_BOT
from tests.telegram_bot.helpers import MINIMALIZED_REQUEST_WITHOUT_TEXT
from tests.telegram_bot.helpers import MINIMALIZED_REQUEST_NOT_ADDRESSED_TO_BOT
from tests.telegram_bot.helpers import MINIMALIZED_REQUEST_WITH_NO_BOOK_PARAMETERS
from tests.telegram_bot.helpers import MINIMALIZED_REQUEST_FOR_BOOK_THAT_DOES_NOT_EXIST

class TestBotEndpointUsingPostRequestView(TestCase):
    def setUp(self):
        self.client = Client()
        self.bot_endpoint_url = reverse("bot-endpoint")

        self.book_create_url = reverse("dashboard-upload-book")

        with open(ABSOLUTE_FILE_PATH) as file_object:
            data = {
                "title": "Demo",
                "author": "Demo",
                "subject": "Demo Subject",
                "details": "Demo Details",
                "file": file_object
            }
            response = self.client.post(self.book_create_url, data)

            data = {
                "title": "Demo",
                "author": "Demo",
                "subject": "Different Demo Subject",
                "details": "Different Demo Details",
                "file": file_object
            }
            response = self.client.post(self.book_create_url, data)

    def test_bot_endpoint_with_valid_chat(self):
        response = self.client.post(self.bot_endpoint_url,
                                    json.dumps(MINIMALIZED_ACCEPTABLE_TELEGRAM_REQUEST),
                                    content_type="application/json")

        self.assertJSONEqual(
            str(response.content, encoding='utf8'),
            {"ok": "Books sent"}
        )

    def test_bot_endpoint_with_message_entirely_missing(self):
        response = self.client.post(self.bot_endpoint_url,
                                    json.dumps(MINIMALIZED_REQUEST_WITHOUT_MESSAGE),
                                    content_type="application/json")

        self.assertJSONEqual(
            str(response.content, encoding='utf8'),
            {"error": "Message missing"}
        )

    def test_bot_endpoint_with_no_chat_id(self):
        response = self.client.post(self.bot_endpoint_url,
                                    json.dumps(MINIMALIZED_REQUEST_WITHOUT_CHAT_ID),
                                    content_type="application/json")

        self.assertJSONEqual(
            str(response.content, encoding='utf8'),
            {"error": "Request is malformed. Key field is missing"}
        )

    def test_bot_endpoint_with_chat_id_not_allowed(self):
        response = self.client.post(self.bot_endpoint_url,
                                    json.dumps(MINIMALIZED_REQUEST_WITH_CHAT_ID_NOT_ALLOWED),
                                    content_type="application/json")

        self.assertJSONEqual(
            str(response.content, encoding='utf8'),
            {"error": "Message not allowed from chat id to bot"}
        )

    def test_bot_endpoint_check_message_is_not_from_a_bot(self):
        response = self.client.post(self.bot_endpoint_url,
                                    json.dumps(MINIMALIZED_REQUEST_FROM_A_BOT),
                                    content_type="application/json")

        self.assertJSONEqual(
            str(response.content, encoding='utf8'),
            {"error": "Messages from other bots are forbidden"}
        )

    def test_bot_endpoint_with_message_having_no_text(self):
        response = self.client.post(self.bot_endpoint_url,
                                    json.dumps(MINIMALIZED_REQUEST_WITHOUT_TEXT),
                                    content_type="application/json")

        self.assertJSONEqual(
            str(response.content, encoding='utf8'),
            {"error": "Message does not have text"}
        )

    def test_bot_endpoint_bot_is_not_being_addressed(self):
        response = self.client.post(self.bot_endpoint_url,
                                    json.dumps(MINIMALIZED_REQUEST_NOT_ADDRESSED_TO_BOT),
                                    content_type="application/json")

        self.assertJSONEqual(
            str(response.content, encoding='utf8'),
            {"error": "Message is not addressed to bot"}
        )

    def test_no_book_parameter_specified(self):
        response = self.client.post(self.bot_endpoint_url,
                                    json.dumps(MINIMALIZED_REQUEST_WITH_NO_BOOK_PARAMETERS),
                                    content_type="application/json")

        self.assertJSONEqual(
            str(response.content, encoding='utf8'),
            {"error": "Message contains no book parameter"}
        )

    def test_matching_books_not_found(self):
        response = self.client.post(self.bot_endpoint_url,
                                    json.dumps(MINIMALIZED_REQUEST_FOR_BOOK_THAT_DOES_NOT_EXIST),
                                    content_type="application/json")

        self.assertJSONEqual(
            str(response.content, encoding='utf8'),
            {"error": "No book matched your query"}
        )
