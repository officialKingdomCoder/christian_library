from collections.abc import MutableSequence, MutableMapping


class State:
    def __init__(self, pattern):
        self.pattern = pattern

    def __str__(self):
        return self.pattern

    def __repr__(self):
        return "<{0} {1}>\n".format(self.__class__.__name__, self.pattern)


class FieldState(State):
    def __init__(self, pattern, field_name):
        super().__init__(pattern)
        self.field_name = field_name

    def __repr__(self):
        return "<{0} {1}>\n".format(self.__class__.__name__, self.field_name)


class Sequence(MutableSequence):
    def __init__(self, data=()):
        self.list = []
        self.extend(data)

    def __getitem__(self, index):
        return self.list[index]

    def __setitem__(self, index, value):
        if not (isinstance(value, State) or isinstance(value, Switch)):
            raise ValueError("value must be a state or switch")
        self.list[index] = value

    def __delitem__(self, index):
        del self.list[index]

    def __len__(self):
        return len(self.list)

    def insert(self, index, value):
        if not (isinstance(value, State) or isinstance(value, Switch)):
            raise ValueError("value must be a state or switch")
        self.list.insert(index, value)

    def __str__(self):
        return str(self.list)

    def __repr__(self):
        return "<{0} {1}>".format(self.__class__.__name__, self.list)


class Switch(MutableMapping):
    def __init__(self, data=()):
        self.mapping = {}
        self.update(data)

    def __getitem__(self, key):
        return self.mapping[key]

    def __delitem__(self, key):
        del self.mapping[key]

    def __setitem__(self, key, value):
        if not isinstance(key, State):
            raise ValueError("key must be a state")
        if not isinstance(value, Sequence) or isinstance(value, Switch):
            raise ValueError("value must be a sequence or switch")
        self.mapping[key] = value

    def __iter__(self):
        return iter(self.mapping)

    def __len__(self):
        return len(self.mapping)

    def __repr__(self):
        return "<{0}>".format(self.mapping)
