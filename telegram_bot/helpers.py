import json
import requests
import re as regex

from django.core import serializers
from django.urls import reverse

from telegram_bot.setup import ALLOWED_CHATS, SEND_MESSAGE_URL, BOT_NAME
from telegram_bot.setup import ACCEPT_MULTI_WORD_FIELDS_AS_QUOTATIONS as MULTI_WORD_FIELDS_ARE_ACCEPTED
from telegram_bot.setup import REGEX_PATTERN_TO_SPLIT_MESSAGE_TEXT_INTO_WORDS as WORD_PATTERN
from telegram_bot.setup import REGEX_PATTERN_TO_IDENTIFY_WORDS_IN_QOUTES as QUOTATION_PATTERN
from telegram_bot.setup import NAME_OF_BOOK_TABLE_IN_DATABASE
from telegram_bot.setup import BOOK_MODEL


from telegram_bot.exceptions import MessageIsMissing
from telegram_bot.exceptions import ChatIdNotAllowed
from telegram_bot.exceptions import InvalidRequest
from telegram_bot.exceptions import MessagesFromBotForbidden
from telegram_bot.exceptions import MessageDoesNotHaveText
from telegram_bot.exceptions import BotIsNotBeingAddressed
from telegram_bot.exceptions import NoBookParameterSpecified
from telegram_bot.exceptions import MatchingBooksNotFound

from message_parsing_automaton.main import extract_fields_using_automaton


def validate_message_is_present(request):
    telegram_bot_api_parameters_passed_as_json_with_request = json.loads(request.body)
    message_field_in_json_received = telegram_bot_api_parameters_passed_as_json_with_request.get("message")

    if not message_field_in_json_received:
        raise MessageIsMissing


def get_relevant_parameters_from_telegram_message(request):
    telegram_bot_api_parameters_passed_as_json_with_request = json.loads(request.body)

    relevant_parameters = {}
    message_field_in_json_received = telegram_bot_api_parameters_passed_as_json_with_request.get("message")

    relevant_parameters["chat_id"] = message_field_in_json_received.get("chat").get("id")
    relevant_parameters["text"] = message_field_in_json_received.get("text")
    relevant_parameters["message_id"] = message_field_in_json_received.get("message_id")
    relevant_parameters["sender_id"] = message_field_in_json_received.get("from").get("id")
    relevant_parameters["is_bot"] = message_field_in_json_received.get("from").get("is_bot")
    relevant_parameters["date"] = message_field_in_json_received.get("date")

    relevant_parameters = {key: value for (key, value) in relevant_parameters.items() if value is not None}

    return relevant_parameters


def validate_chat_id(telegram_parameters):
    if "chat_id" not in telegram_parameters:
        raise InvalidRequest

    if telegram_parameters["chat_id"] not in ALLOWED_CHATS:
        raise ChatIdNotAllowed(telegram_parameters["chat_id"])


def send_chat_id_not_allowed_error_message_to_telegram(chat_id):
    return_text = "I'm sorry.\n\n"
    return_text += "You are not allowed to chat with me here.\n"
    return_text += "I'd be glad to respond to you if you chat me up on the Ilanaa Demo group chat.\n"
    return_text += "You can use this link to join:\n\n"
    return_text += "https://t.me/joinchat/OpPw802Ulr_AcCtDxS-thw"

    send_response_message_to_telegram(chat_id, return_text)


def send_response_message_to_telegram(chat_id, return_text):
    json_response = {
        "chat_id": chat_id,
        "text": return_text,
    }

    requests.post(SEND_MESSAGE_URL, json=json_response)


def validate_message_is_not_from_another_bot(telegram_parameters):
    if "is_bot" not in telegram_parameters:
        raise InvalidRequest

    if telegram_parameters["is_bot"] is True:
        raise MessagesFromBotForbidden


def validate_message_has_text(telegram_parameters):
    if "text" not in telegram_parameters:
        raise MessageDoesNotHaveText


def extract_fields_from_message_text(telegram_parameters):
    message_text = telegram_parameters["text"]
    message_text = message_text.lower()

    word_tokens_extracted_from_message_text = tokenize(message_text)

    fields_in_message_text = extract_fields_using_automaton(word_tokens_extracted_from_message_text)

    return fields_in_message_text


def tokenize(text):
    tokens = []
    if MULTI_WORD_FIELDS_ARE_ACCEPTED:
        tokens = extract_single_words_and_multi_word_wrapped_in_quotes_from_text(text)
    else:
        tokens = extract_single_words_from_text(text)

    tokens = [token for token in tokens if token != ""]
    return tokens


def extract_single_words_and_multi_word_wrapped_in_quotes_from_text(text):
    quotations_found_in_text = regex.findall(QUOTATION_PATTERN, text)
    quotations_found_in_text = strip_single_and_double_quote_symbols(quotations_found_in_text)

    if not quotations_found_in_text:
        words = extract_single_words_from_text(text)
        return words

    else:
        substrings_splitted_out_of_text_based_on_quotation_pattern = regex.split(QUOTATION_PATTERN, text)
        words_and_quotations = generate_tokens_based_on_substrings_and_quotations(
                substrings_splitted_out_of_text_based_on_quotation_pattern,
                quotations_found_in_text
            )
        return words_and_quotations


def strip_single_and_double_quote_symbols(quotations):
    quotations_stripped_of_symbol = []
    for quotation in quotations:
        quotation = quotation.strip("'")
        quotation = quotation.strip('"')
        quotations_stripped_of_symbol.append(quotation)
    return quotations_stripped_of_symbol


def extract_single_words_from_text(text):
    word_tokens_in_text = regex.split(WORD_PATTERN, text)
    words_in_text = [token for token in word_tokens_in_text if token != ""]
    return words_in_text


def generate_tokens_based_on_substrings_and_quotations(substrings, quotations):
    all_words_grouped_by_substrings = []

    for substring in substrings:
        words = extract_single_words_and_multi_word_wrapped_in_quotes_from_text(substring)
        all_words_grouped_by_substrings.append(words)

    tokens = combine_quotations_and_words_grouped_by_substrings_into_tokens(quotations, all_words_grouped_by_substrings)

    return tokens


def combine_quotations_and_words_grouped_by_substrings_into_tokens(quotations, words_grouped_by_substrings):
    combination_of_quotations_and_words_grouped_by_substrings = [None]*(len(quotations)+len(words_grouped_by_substrings))

    combination_of_quotations_and_words_grouped_by_substrings[::2] = words_grouped_by_substrings
    combination_of_quotations_and_words_grouped_by_substrings[1::2] = quotations

    tokens = remove_word_groupings(combination_of_quotations_and_words_grouped_by_substrings)
    return tokens


def remove_word_groupings(combination):
    nested_list = convert_every_non_list_item_into_a_list(combination)

    flat_list = [item for sublist in nested_list for item in sublist]
    return flat_list


def convert_every_non_list_item_into_a_list(heterogenous_list):
    list_of_lists = []
    for item in heterogenous_list:
        if type(item) is not list:
            list_of_lists.append([item])
        else:
            list_of_lists.append(item)
    return list_of_lists


def validate_bot_is_being_addressed(fields_in_message_text):
    if "bot_name" not in fields_in_message_text:
        raise BotIsNotBeingAddressed(BOT_NAME)

    elif fields_in_message_text["bot_name"] != BOT_NAME.lower():
        raise BotIsNotBeingAddressed(BOT_NAME)


def validate_book_parameters_were_specified(fields_in_message_text):
    if not fields_in_message_text:
        raise NoBookParameterSpecified


def convert_queryset_to_list_of_dictionaries(database_queryset):
    matching_books = serializers.serialize("python", database_queryset)
    return matching_books


def find_books_in_database(fields):
    sql_query = generate_sql_query_from_search_fields(fields)

    database_query_results = BOOK_MODEL.objects.raw(sql_query)

    return database_query_results


def generate_sql_query_from_search_fields(fields):
    where_clause = generate_where_clause(fields)

    full_query = "SELECT * FROM {table} {where_clause};".format(table=NAME_OF_BOOK_TABLE_IN_DATABASE, where_clause=where_clause)

    return full_query


def generate_where_clause(fields):
    conditions = ""
    for field in fields:
        conditions += "AND LOWER({column}) LIKE LOWER('%%{pattern}%%') ".format(column=field, pattern=fields[field])

    conditions = conditions.lstrip("AND ")
    conditions = conditions.rstrip(" ")

    where_clause = "WHERE {conditions}".format(conditions=conditions)
    return where_clause


def remove_bot_name_from_fields(fields):
    return {key: value for (key, value) in fields.items() if key != "bot_name"}


def send_no_matching_books_found_error_message_to_telegram(chat_id, parameters):
    return_text = "I'm sorry.\n\n"
    return_text += "I found no book matching these parameters:\n"
    return_text += generate_return_text_from_parameters(parameters)

    send_response_message_to_telegram(chat_id, return_text)


def generate_return_text_from_parameters(parameters):
    return_text = ""
    for parameter in parameters:
        return_text += "{key}: {value}\n".format(key=parameter, value=parameters[parameter])
    return return_text


def validate_books_that_matched_query_were_found(matching_books, telegram_parameters, book_parameters):
    if not matching_books:
        raise MatchingBooksNotFound(telegram_parameters["chat_id"], book_parameters)


def send_link_to_url_of_matching_books_to_telegram(request, matching_books, telegram_parameters, fields_in_message_text):
    more_than_one_book = len(matching_books) > 1

    if more_than_one_book:
        print("# DEBUG: More than one book")
        send_link_to_url_of_list_of_books(request, telegram_parameters, fields_in_message_text)

    else:
        print("# DEBUG: One book")
        print(matching_books)
        slug = matching_books[0]["fields"]["slug"]
        send_link_to_url_of_details_of_matching_book(request, slug, telegram_parameters)


def send_link_to_url_of_list_of_books(request, telegram_parameters, fields_in_message_text):
    url_parameters = generate_url_parameters(fields_in_message_text)

    relative_url_of_listview_for_search_results = reverse("search-results")
    full_url_of_listview_for_search_results = request.build_absolute_uri(relative_url_of_listview_for_search_results)

    url_for_this_query = "{full_url}?{url_parameters}".format(
                                                              full_url=full_url_of_listview_for_search_results,
                                                              url_parameters=url_parameters
                                                              )

    send_link_to_url_to_telegram(url_for_this_query, telegram_parameters)


def generate_url_parameters(fields_in_message_text):
    url_parameters = ""
    for field in fields_in_message_text:
        value = fields_in_message_text[field].replace(" ", "+")
        url_parameters += "&{key}={value}".format(key=field, value=value)
    url_parameters = url_parameters.strip("&")
    return url_parameters


def send_link_to_url_to_telegram(url_to_view, telegram_parameters):
    chat_id = telegram_parameters["chat_id"]
    return_text = "Your book are here:\n"
    return_text += url_to_view

    send_response_message_to_telegram(chat_id, return_text)


def send_link_to_url_of_details_of_matching_book(request, slug, telegram_parameters):
    relative_url_of_detailview_for_book_found = reverse("book-details", kwargs={"slug": slug})
    full_url_of_detailview_for_book_found = request.build_absolute_uri(relative_url_of_detailview_for_book_found)
    print(full_url_of_detailview_for_book_found)

    send_link_to_url_to_telegram(full_url_of_detailview_for_book_found, telegram_parameters)
