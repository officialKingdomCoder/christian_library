from django.test import TransactionTestCase


class TestBookModel(TransactionTestCase):
    pass

    # TODO: I need to find a way to test that the file
    # in the FileField is being stored in the correct location
    # The problem is that I am getting an author_title_path object back,
    # not the string.

    # TODO: Test Slug
    # There is no EXPLICIT test for my slugify function.
    # However, the fact that my Update and Delete Views are working
    # is an implicit test since they depend on having an accurate slug
