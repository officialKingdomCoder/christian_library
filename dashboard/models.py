from django.db import models
from django.utils.text import slugify


class Book(models.Model):
    title = models.CharField(max_length=50)
    slug = models.SlugField()
    author = models.CharField(max_length=100)
    subject = models.CharField(max_length=50)
    details = models.CharField(max_length=255)

    def author_title_path(self, filename):
        return "files/{author}/{filename}".format(author=slugify(self.author), filename=filename)

    file = models.FileField(upload_to=author_title_path)

    def save(self, *args, **kwargs):
        title_by_author = "{title} by {author}".format(title=self.title, author=self.author)
        self.slug = slugify(title_by_author)
        super().save(*args, **kwargs)
