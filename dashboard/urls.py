from django.urls import path
from dashboard.views import BookListView, BookCreateView, BookUpdateView, BookDeleteView

urlpatterns = [
    path("list_books/", BookListView.as_view(), name="dashboard-list-books"),
    path("add_book/", BookCreateView.as_view(), name="dashboard-upload-book"),
    path("edit_book/<slug>/", BookUpdateView.as_view(), name="dashboard-edit-book"),
    path("delete_book/<slug>/", BookDeleteView.as_view(), name="dashboard-delete-book")
]
