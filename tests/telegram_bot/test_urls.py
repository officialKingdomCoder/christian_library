from django.test import SimpleTestCase
from django.urls import reverse, resolve


class TestTelegramBotEndpointURL(SimpleTestCase):
    def test_telegram_bot_endpoint_url_resolves(self):
        bot_endpoint_name = "bot-endpoint"
        bot_endpoint_url = reverse(bot_endpoint_name)

        self.assertURLEqual(resolve(bot_endpoint_url).view_name, bot_endpoint_name)
